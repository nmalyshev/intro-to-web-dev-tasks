/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-undef */
const render = () => {
    document.querySelector('.index').style.display = "block";
    document.querySelector('.feature').style.display = "none";
    document.querySelector('.sign').style.display = "none";
    document.querySelector('body').style.background = "url('assets/images/background-page-landing.png')";
};

const renderfeature = () => {
    document.querySelector('.index').style.display = "none";
    document.querySelector('.feature').style.display = "block";
    document.querySelector('.sign').style.display = "none";
    document.querySelector('body').style.background = "#DBAC74";
};

const rendersign = () => {
    document.querySelector('.index').style.display = "none";
    document.querySelector('.feature').style.display = "none";
    document.querySelector('.sign').style.display = "block";
    document.querySelector('body').style.backgroundImage = "url('assets/images/background-page-sign-up.png')";
};


render()